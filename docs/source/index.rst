:notoc:

.. pyoccad documentation master file, created by

.. module:: pyoccad

*********************
Pyoccad documentation
*********************

**Date**: |today| **Version**: |version|

**Useful links**:
`Source Repository <https://gitlab.com/cosapp/pyoccad>`__ |
`Issues & Ideas <https://gitlab.com/cosapp/pyoccad/-/issues/>`__

:mod:`pyoccad` is a set of helpers to ease creation and manipulation of OpenCascade CAD library.

.. raw:: html

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 d-flex">
                <div class="card text-center intro-card shadow">
                <img src="_static/index_getting_started.svg" class="card-img-top" alt="getting started with Pyoccad action icon" height="52">
                <div class="card-body flex-fill">
                    <h5 class="card-title">Getting started</h5>
                    <p class="card-text">New to <em>Pyoccad</em>?<br>Check out the getting started guide.</p>

.. container:: custom-button

    :ref:`To the getting started guide<getting_started>`

.. raw:: html

                </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 d-flex">
                <div class="card text-center intro-card shadow">
                <img src="_static/index_user_guide.svg" class="card-img-top" alt="Pyoccad user guide action icon" height="52">
                <div class="card-body flex-fill">
                    <h5 class="card-title">User guide</h5>
                    <p class="card-text">In-depth information on how to use <em>Pyoccad</em>.</p>

.. container:: custom-button

    :ref:`To the user guide<user_guide>`

.. raw:: html

                </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 d-flex">
                <div class="card text-center intro-card shadow">
                <img src="_static/index_api.svg" class="card-img-top" alt="api of Pyoccad action icon" height="52">
                <div class="card-body flex-fill">
                    <h5 class="card-title">API reference</h5>
                    <p class="card-text">A detailed description of the <em>Pyoccad</em> API.</p>

.. container:: custom-button

    :ref:`To the reference guide<api>`

.. raw:: html

                </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 d-flex">
                <div class="card text-center intro-card shadow">
                <img src="_static/index_contribute.svg" class="card-img-top" alt="contribute to Pyoccad action icon" height="52">
                <div class="card-body flex-fill">
                    <h5 class="card-title">Developer guide</h5>
                    <p class="card-text">A guide through the process of improving <em>Pyoccad</em>.</p>

.. container:: custom-button

    :ref:`To the development guide<development>`

.. raw:: html

                </div>
                </div>
            </div>
        </div>
    </div>


.. toctree::
    :maxdepth: 4
    :hidden:
    :titlesonly:


    getting_started/index
    user_guide/index
    api/index
    development/index