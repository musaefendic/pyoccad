.. _pyoccad.render:

pyoccad.render
==============

.. _pyoccad.render.JupyterThreeJSRenderer:

JupyterThreeJSRenderer
----------------------
.. autoclass:: pyoccad.render.JupyterThreeJSRenderer
    :members:

.. _pyoccad.render.JupyterThreeJSRenderer2d:

JupyterThreeJSRenderer2d
------------------------
.. autoclass:: pyoccad.render.JupyterThreeJSRenderer2d
    :members: