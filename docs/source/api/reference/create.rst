.. _pyoccad.create:

pyoccad.create
==============

.. _pyoccad.create.CreateArc:

Arc
---
.. autoclass:: pyoccad.create.CreateArc
    :members:

.. _pyoccad.create.CreateArray1:

Array1
------
.. autoclass:: pyoccad.create.CreateArray1
    :members:

.. _pyoccad.create.CreateArray2:

Array2
------
.. autoclass:: pyoccad.create.CreateArray2
    :members:

.. _pyoccad.create.CreateAxis:

Axis
----
.. autoclass:: pyoccad.create.CreateAxis
    :members:

.. _pyoccad.create.CreateBSpline:

BSpline
-------
.. autoclass:: pyoccad.create.CreateBSpline
    :members:

.. _pyoccad.create.CreateBezier:

Bezier
------
.. autoclass:: pyoccad.create.CreateBezier
    :members:

.. _pyoccad.create.CreateBezierSurface:

BezierSurface
-------------
.. autoclass:: pyoccad.create.CreateBezierSurface
    :members:

.. _pyoccad.create.CreateBox:

Box
---
.. autoclass:: pyoccad.create.CreateBox
    :members:

.. _pyoccad.create.CreateCircle:

Circle
------
.. autoclass:: pyoccad.create.CreateCircle
    :members:

.. _pyoccad.create.CreateCone:

Cone
----
.. autoclass:: pyoccad.create.CreateCone
    :members:

.. _pyoccad.create.CreateConic:

Conic
-----
.. autoclass:: pyoccad.create.CreateConic
    :members:

.. _pyoccad.create.CreateControlPoint:

ControlPoint
------------
.. autoclass:: pyoccad.create.CreateControlPoint
    :members:

.. _pyoccad.create.CreateCoordSystem:

CoordSystem
-----------
.. autoclass:: pyoccad.create.CreateCoordSystem
    :members:

.. _pyoccad.create.CreateCurve:

Curve
-----
.. autoclass:: pyoccad.create.CreateCurve
    :members:

.. _pyoccad.create.CreateDirection:

Direction
---------
.. autoclass:: pyoccad.create.CreateDirection
    :members:

.. _pyoccad.create.CreateEdge:

Edge
----
.. autoclass:: pyoccad.create.CreateEdge
    :members:

.. _pyoccad.create.CreateEllipse:

Ellipse
-------
.. autoclass:: pyoccad.create.CreateEllipse
    :members:

.. _pyoccad.create.CreateExtrusion:

Extrusion
---------
.. autoclass:: pyoccad.create.CreateExtrusion
    :members:

.. _pyoccad.create.CreateFace:

Face
----
.. autoclass:: pyoccad.create.CreateFace
    :members:

.. _pyoccad.create.CreateHArray1:

HArray1
-------
.. autoclass:: pyoccad.create.CreateHArray1
    :members:

.. _pyoccad.create.CreateInterpolation:

Interpolation
-------------
.. autoclass:: pyoccad.create.CreateInterpolation
    :members:

.. _pyoccad.create.CreateIntersection:

Intersection
------------
.. autoclass:: pyoccad.create.CreateIntersection
    :members:

.. _pyoccad.create.CreateLine:

Line
----
.. autoclass:: pyoccad.create.CreateLine
    :members:

.. _pyoccad.create.CreateList:

List
----
.. autoclass:: pyoccad.create.CreateList
    :members:

.. _pyoccad.create.CreateOCCList:

OCCList
-------
.. autoclass:: pyoccad.create.CreateOCCList
    :members:

.. _pyoccad.create.CreateOffset:

Offset
------
.. autoclass:: pyoccad.create.CreateOffset
    :members:

.. _pyoccad.create.CreatePlane:

Plane
-----
.. autoclass:: pyoccad.create.CreatePlane
    :members:

.. _pyoccad.create.CreatePoint:

Point
-----
.. autoclass:: pyoccad.create.CreatePoint
    :members:

.. _pyoccad.create.CreateRevolution:

Revolution
----------
.. autoclass:: pyoccad.create.CreateRevolution
    :members:

.. _pyoccad.create.CreateRotation:

Rotation
--------
.. autoclass:: pyoccad.create.CreateRotation
    :members:

.. _pyoccad.create.CreateScaling:

Scaling
-------
.. autoclass:: pyoccad.create.CreateScaling
    :members:

.. _pyoccad.create.CreateSequence:

Sequence
--------
.. autoclass:: pyoccad.create.CreateSequence
    :members:

.. _pyoccad.create.CreateShell:

Shell
-----
.. autoclass:: pyoccad.create.CreateShell
    :members:

.. _pyoccad.create.CreateSolid:

Solid
-----
.. autoclass:: pyoccad.create.CreateSolid
    :members:

.. _pyoccad.create.CreateSphere:

Sphere
------
.. autoclass:: pyoccad.create.CreateSphere
    :members:

.. _pyoccad.create.CreateSurface:

Surface
-------
.. autoclass:: pyoccad.create.CreateSurface
    :members:

.. _pyoccad.create.CreateSymmetry:

Symmetry
--------
.. autoclass:: pyoccad.create.CreateSymmetry
    :members:

.. _pyoccad.create.CreateTopology:

Topology
--------
.. autoclass:: pyoccad.create.CreateTopology
    :members:

.. _pyoccad.create.CreateTranslation:

Translation
-----------
.. autoclass:: pyoccad.create.CreateTranslation
    :members:

.. _pyoccad.create.CreateUnsignedCoordSystem:

UnsignedCoordSystem
-------------------
.. autoclass:: pyoccad.create.CreateUnsignedCoordSystem
    :members:

.. _pyoccad.create.CreateVector:

Vector
------
.. autoclass:: pyoccad.create.CreateVector
    :members:

.. _pyoccad.create.CreateVertex:

Vertex
------
.. autoclass:: pyoccad.create.CreateVertex
    :members:

.. _pyoccad.create.CreateWire:

Wire
----
.. autoclass:: pyoccad.create.CreateWire
    :members: