.. _pyoccad.types:

pyoccad.types
=============

Points
------
.. automodule:: pyoccad.types
   :members: PointT, Point2T, Point3T

Directions
----------
.. automodule:: pyoccad.types
   :members: DirectionT, Direction2T, Direction3T

Vectors
-------
.. automodule:: pyoccad.types
   :members: VectorT, Vector2T, Vector3T, OccVector
