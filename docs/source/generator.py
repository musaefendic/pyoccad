from collections import OrderedDict

from jinja2 import Template

from pyoccad import create, render, transform


with open('_templates/factory_class.template.rst', 'r') as f:
    tm = Template(f.read())
with open('_templates/module.template.rst', 'r') as f:
    module_template = Template(f.read())

for module, shortener in [(create, lambda c: c[6:]),
                          (render, lambda c: c),
                          (transform, lambda c: c),
                          ]:

    short_name = module.__name__.rsplit('.', 1)[-1]
    name = module.__name__
    classes = dict([(name, cls) for name, cls in module.__dict__.items() if isinstance(cls, type)])
    sorted_classes = OrderedDict(sorted(classes.items()))

    sphinx_list = [tm.render(name=c,
                             short_name=shortener(c),
                             module_name=name,
                             separator='-'*len(shortener(c))) for c in sorted_classes]

    with open(f'api/reference/{short_name}.rst', 'w') as f:
        f.write(module_template.render(name=name,
                                       separator='='*len(module.__name__),
                                       classes='\n\n'.join(sphinx_list)))

from pyoccad.create import CreatePoint
CreatePoint.as_point((0., 1., 2.))